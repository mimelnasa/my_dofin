# Generated by Django 2.2.1 on 2019-07-20 19:41

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CamelProduct',
            fields=[
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('id', models.UUIDField(default=uuid.uuid4, help_text='ID único para esta petición concreta.', primary_key=True, serialize=False)),
                ('descripcion', models.CharField(help_text='Descripción principal de producto.', max_length=1000)),
                ('highest_amazon_price', models.FloatField(help_text='Precio máximo del producto de Amazon en Amazon.', null=True)),
                ('highest_amazon_date', models.CharField(help_text='Fecha del precio máximo del producto de Amazon en Amazon.', max_length=100, null=True)),
                ('lowest_amazon_price', models.FloatField(help_text='Precio mínimo del producto de Amazon en Amazon.', null=True)),
                ('lowest_amazon_date', models.CharField(help_text='Fecha del precio mínimo del producto de Amazon en Amazon.', max_length=100, null=True)),
                ('avg_amazon_price', models.FloatField(help_text='Precio medio del producto de Amazon en Amazon.', null=True)),
                ('highest_tercero_price', models.FloatField(help_text='Precio máximo del producto de terceros en Amazon.', null=True)),
                ('highest_tercero_date', models.CharField(help_text='Fecha del precio mínimo del producto de terceros en Amazon.', max_length=100, null=True)),
                ('lowest_tercero_price', models.FloatField(help_text='Precio mínimo del producto de terceros en Amazon.', null=True)),
                ('lowest_tercero_date', models.CharField(help_text='Fecha del precio mínimo del producto de terceros en Amazon.', max_length=100, null=True)),
                ('avg_tercero_price', models.FloatField(help_text='Precio medio del producto de terceros en Amazon.', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Petition',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, help_text='ID único para esta petición concreta.', primary_key=True, serialize=False)),
                ('urlpetition', models.URLField()),
                ('protocol', models.CharField(default=models.URLField(), help_text='Concepto de la URL objeto de estudio.', max_length=10)),
                ('predomain', models.CharField(default=models.URLField(), help_text='Concepto de la URL objeto de estudio.', max_length=100)),
                ('domain', models.CharField(default=models.URLField(), help_text='Concepto de la URL objeto de estudio.', max_length=1000)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('id', models.UUIDField(default=uuid.uuid4, help_text='ID único para esta petición concreta.', primary_key=True, serialize=False)),
                ('descripcion', models.CharField(help_text='Descripción principal de producto.', max_length=1000)),
                ('marca', models.CharField(help_text='Marca del producto.', max_length=1000, null=True)),
                ('modelo', models.CharField(help_text='Modelo del producto.', max_length=1000, null=True)),
                ('precio', models.FloatField(help_text='Precio del producto.', null=True)),
                ('media_valoraciones', models.FloatField(help_text='Media de las valoraciones por los usuarios.', null=True)),
                ('num_valoraciones', models.IntegerField(help_text='Número de valoraciones por los usuarios.', null=True)),
                ('seller_name', models.CharField(help_text='Nombre de usuario del vendedor del producto.', max_length=1000, null=True)),
                ('seller_url', models.URLField(help_text='Perfil del vendedor del producto.', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('id', models.UUIDField(default=uuid.uuid4, help_text='ID único para esta petición concreta.', primary_key=True, serialize=False)),
                ('nombre', models.CharField(help_text='Nombre completo del vendedor del producto.', max_length=1000, null=True)),
                ('direccion', models.CharField(help_text='Dirección del vendedor del producto.', max_length=1000, null=True)),
                ('pais', models.CharField(help_text='País del vendedor del producto.', max_length=1000, null=True)),
                ('antiguedad', models.CharField(help_text='Fecha de alta del vendedor del producto.', max_length=1000, null=True)),
                ('email', models.EmailField(blank=True, help_text='Email del vendedor del producto.', max_length=254, null=True)),
                ('seguidores', models.IntegerField(help_text='Número de seguidores del vendedor.', null=True)),
                ('articulos_en_venta', models.IntegerField(help_text='Número de artículos en venta del vendedor.', null=True)),
                ('feedback_url', models.URLField(help_text='Perfil de votos del vendedor del producto.', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='SellerFeedback',
            fields=[
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('id', models.UUIDField(default=uuid.uuid4, help_text='ID único para esta petición concreta.', primary_key=True, serialize=False)),
                ('user_name', models.CharField(help_text='Nombre de usuario del vendedor del producto.', max_length=1000, null=True)),
                ('indicador_PS', models.CharField(help_text='Indicador de vendedor con categoría de POWER SELLER.', max_length=100, null=True)),
                ('feedback_num_total', models.IntegerField(help_text='Número de valoraciones totales por los usuarios.', null=True)),
                ('num_positives_12months', models.IntegerField(help_text='Número de valoraciones positivas por los usuarios en los últimos 12 meses.', null=True)),
                ('num_negatives_12months', models.IntegerField(help_text='Número de valoraciones neutrales por los usuarios en los últimos 12 meses.', null=True)),
                ('num_neutrals_12months', models.IntegerField(help_text='Número de valoraciones negativas por los usuarios en los últimos 12 meses.', null=True)),
                ('num_positives_6months', models.IntegerField(help_text='Número de valoraciones positivas por los usuarios en los últimos 6 meses.', null=True)),
                ('num_negatives_6months', models.IntegerField(help_text='Número de valoraciones neutrales por los usuarios en los últimos 6 meses.', null=True)),
                ('num_neutrals_6months', models.IntegerField(help_text='Número de valoraciones negativas por los usuarios en los últimos 6 meses.', null=True)),
                ('num_positives_1month', models.IntegerField(help_text='Número de valoraciones positivas por los usuarios en el último mes.', null=True)),
                ('num_negatives_1month', models.IntegerField(help_text='Número de valoraciones neutrales por los usuarios en el último mes.', null=True)),
                ('num_neutrals_1month', models.IntegerField(help_text='Número de valoraciones negativas por los usuarios en el último mes.', null=True)),
            ],
        ),
    ]
