from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _ # Para futuras traducciones
from .models import Petition 

class PetitionModelForm(forms.ModelForm):
	class Meta:
		model = Petition
		fields = ["urlpetition"]		
		labels = {'urlpetition': 'Introduce a URL'}
		placeholder = {'urlpetition': 'Introduce a URL'}
		help_texts = {'La URL debe comenzar por HTTP o HTTPS'}

	def clean_urlpetition(self):
		url = self.cleaned_data.get("urlpetition")

		# Solo permite protocolo http[s] o NADA
		if '://' in url and not url.startswith('http') :
			raise ValidationError(_('Sólo se admite https y http.'))
		elif not 'ebay' in url.split('//')[1].split('/')[0]:
			raise ValidationError(_('Por el momento sólo se admiten productos de eBay.'))
		return url


class ContactForm(forms.Form):
	name = forms.CharField(required=False)
	email = forms.EmailField()
	subject = forms.CharField(required=False)
	message = forms.CharField(widget=forms.Textarea)
