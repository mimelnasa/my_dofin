from django.apps import AppConfig


# class RegistrationConfig(AppConfig):
#     name = 'registration'

# class CrispyForms(AppConfig):
#     name = 'crispy_forms'

class InspectorConfig(AppConfig):
    name = 'inspector'
