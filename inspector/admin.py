from django.contrib import admin

# Importar modelos:
from .models import Petition, Product, Seller, SellerFeedback, CamelProduct

#Importar formularios:
#from .forms import PetitionModelForm

# Register your models here.

class AdminPetition(admin.ModelAdmin):
	list_display = ["domain","urlpetition","timestamp"]
	list_filter = ["timestamp"]
	search_fields = ["email"]
	readonly_fields = ('timestamp',)


class AdminProduct(admin.ModelAdmin):
	list_display = ["descripcion","precio","seller_name","timestamp"]
	readonly_fields = ('timestamp',)


class AdminSeller(admin.ModelAdmin):
	list_display = ["nombre","email","feedback_url","timestamp"]
	readonly_fields = ('timestamp',)


class AdminSellerFeedback(admin.ModelAdmin):
	list_display = ["user_name","indicador_PS","timestamp"]
	readonly_fields = ('timestamp',)


class AdminCamelProduct(admin.ModelAdmin):
	list_display = ["descripcion","timestamp"]
	readonly_fields = ('timestamp',)


admin.site.register(Petition, AdminPetition)
admin.site.register(Product, AdminProduct)
admin.site.register(Seller, AdminSeller)
admin.site.register(SellerFeedback, AdminSellerFeedback)
admin.site.register(CamelProduct, AdminCamelProduct)
