# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import re

class RastreathorPipeline(object):

	def process_item(self, item, spider):
		if spider.name == 'crawl_seller':
			item['ind_no_foto'] = int(float(item['ind_no_foto']))
			
		elif spider.name == 'crawl_sellerfeedback':
			item['indicador_PS'] = int(float(item['indicador_PS']))

		elif spider.name == 'crawl_product':
			item['marca'] = re.sub('[-_/]', '', item['marca'])
			# item['modelo'] = re.sub('[-_/]', '', item['modelo'])
			item['precio'] = -1.0 if item['precio']==-1.0 else float(re.sub('[^0-9,.]', '', item['precio']).replace('.','').replace(',','.')) 
			item['divisa'] = re.sub('[0-9,. ]', '', item['divisa']) or ''
			item['vendidos'] = int(re.sub('[^0-9]', '', item['vendidos']) or -1)
			item['media_valoraciones'] = item['media_valoraciones'] or -1.0
			item['term_return'] = int(re.sub('[^0-9]', '', item['term_return']))
			# item['shipping_costs'] = -1.0 if item['shipping_costs']==-1.0 else float(re.sub('[^0-9,.]', '', item['shipping_costs']).replace('.','').replace(',','.')) 
			item['ind_paypal'] = int(float(item['ind_paypal']))
			item['ind_mastercard'] = int(float(item['ind_mastercard']))
			item['ind_visa'] = int(float(item['ind_visa']))
			item['ind_discover'] = int(float(item['ind_discover']))

		elif spider.name == 'crawl_camel':
			item['ind_NO_PRODUCT'] = int(float(item['ind_NO_PRODUCT']))
			if not item['ind_NO_PRODUCT'] :
				item['highest_amazon_price'] = float(re.sub('[^0-9,.]', '', item['highest_amazon_price']).replace('.','').replace(',','.') or -1.0)
				item['lowest_amazon_price'] = float(re.sub('[^0-9,.]', '', item['lowest_amazon_price']).replace('.','').replace(',','.') or -1.0)
				item['avg_amazon_price'] = float(re.sub('[^0-9,.]', '', item['avg_amazon_price']).replace('.','').replace(',','.') or -1.0)
				item['highest_tercero_price'] = float(re.sub('[^0-9,.]', '', item['highest_tercero_price']).replace('.','').replace(',','.') or -1.0)
				item['lowest_tercero_price'] = float(re.sub('[^0-9,.]', '', item['lowest_tercero_price']).replace('.','').replace(',','.') or -1.0)
				item['avg_tercero_price'] = float(re.sub('[^0-9,.]', '', item['avg_tercero_price']).replace('.','').replace(',','.') or -1.0)
				if item['SKU_graph']:
					item['URL_graph'] = 'https://charts.camelcamelcamel.com/es/'+ item['SKU_graph'] +'/amazon-new.png?force=1&zero=0&w=855&h=513&desired=false&legend=1&ilt=1&tp=all&fo=0&lang=es_ES'
					print("*************************************************************")
					print(item['URL_graph'])
					print("*************************************************************")


		item.save()
		
		return item

