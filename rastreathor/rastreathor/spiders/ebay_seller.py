
from scrapy.utils.project import get_project_settings
from scrapy.spiders import Spider, Rule
from scrapy.linkextractors import LinkExtractor
from rastreathor.items import SellerItem

class EbaySellerSpider(Spider):
	name = 'crawl_seller'

	def __init__(self, *args, **kwargs):
		self.url = kwargs.get('url')
		self.domain = kwargs.get('domain')	
		self.id = kwargs.get('id')       
		self.start_urls = [self.url]
		self.allowed_domains = [self.domain]

 #        IcrawlerSpider.rules = [
 #           Rule(LinkExtractor(unique=True), callback='parse_item'),
 #        ]
 #        super(IcrawlerSpider, self).__init__(*args, **kwargs)

	# rules = {
	# # 	# Para cada item
	#  	Rule(LinkExtractor(unique=True,
	# 						callback = 'parse_items', follow = True))
	# }

	def parse(self, response):
		new_seller_item = SellerItem()
		#info eBay
		print("ESTOY PARSEANDO SELLER")
		print("============ID================%s" %self.id)
		new_seller_item['id'] = self.id		
		new_seller_item['nombre'] = response.xpath('normalize-space(//*[@id="business_name"]/following-sibling::*[1])').get() or  "Name Not Found"
		new_seller_item['direccion'] = response.xpath('normalize-space(//*[@id="address"]/following-sibling::*[1])').get() 
		new_seller_item['pais'] = response.xpath('normalize-space(//*[@class="mem_loc"]/text())').get() 
		new_seller_item['antiguedad'] = response.xpath('normalize-space(//*[@class="mem_loc"]/preceding-sibling::*[3]/*[@class="info"])').get() 
		new_seller_item['email'] = response.xpath('normalize-space(//*[@id="email"]/following-sibling::*[1])').get() 
		new_seller_item['seguidores'] = response.xpath('normalize-space(//*[@class="follwrs"]//span/a/text())').get() or -1
		new_seller_item['articulos_en_venta'] = response.xpath('normalize-space(//*[@class="sell_count"]//a/text())').get() or -1
		new_seller_item['feedback_url'] = response.xpath('normalize-space(//*[@class="all_fb fr"]//a/@href)').get() or -1
		new_seller_item['ind_no_foto'] = response.xpath('count(//div[@id="index_card"]//img[@src="https://ir.ebaystatic.com/pictures/aw/social/avatar.png"])').get()

		return new_seller_item

