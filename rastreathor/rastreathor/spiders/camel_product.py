
from scrapy.utils.project import get_project_settings
from scrapy.spiders import Spider, Rule
from scrapy.linkextractors import LinkExtractor
from rastreathor.items import CamelItem
from scrapy.http.request import Request

class CamelProductSpider(Spider):
	name = 'crawl_camel'

	def __init__(self, *args, **kwargs):
		self.url = kwargs.get('url')
		self.domain = kwargs.get('domain')	
		self.id = kwargs.get('id')       
		self.start_urls = [self.url]
		# self.allowed_domains = [self.domain]

 #        IcrawlerSpider.rules = [
 #           Rule(LinkExtractor(unique=True), callback='parse_item'),
 #        ]
 #        super(IcrawlerSpider, self).__init__(*args, **kwargs)

	# rules = {
	# # 	# Para cada item
	#  	Rule(LinkExtractor(unique=True,
	# 						callback = 'parse_items', follow = True))
	# }

	def parse(self, response):
		url_product = response.xpath('//div[@class="row column search_results"]//a/@href').get() or self.url		
		not_found = response.xpath('count(//p[contains(text(),"any results for your search")])').get()
		if not_found == '0.0':
			yield Request(url_product, callback = self.parse_extra)			
		else:
			new_camel_item = CamelItem()
			new_camel_item['id'] = self.id	
			new_camel_item['ind_NO_PRODUCT'] = not_found
			yield new_camel_item

            

	def parse_extra(self, response):
		new_camel_item = CamelItem()
		#info CamelCamelCamel
		print("ESTOY PARSEANDO CAMEL")
		print("============ID================%s" %self.id)
		new_camel_item['id'] = self.id	
		new_camel_item['descripcion'] = response.xpath('normalize-space(//div[@class="row column show-for-medium"]//a/text())').get()
		new_camel_item['ind_NO_PRODUCT'] = response.xpath('count(//p[contains(text(),"any results for your search")])').get()
		new_camel_item['highest_amazon_price'] = response.xpath('normalize-space(//*[@class="pricetype pricetype0"]/following-sibling::*//tr[@class="highest_price"]/td[2])').get() 
		new_camel_item['highest_amazon_date'] = response.xpath('normalize-space(//*[@class="pricetype pricetype0"]/following-sibling::*//tr[@class="highest_price"]/td[3])').get()
		new_camel_item['lowest_amazon_price'] = response.xpath('normalize-space(//*[@class="pricetype pricetype0"]/following-sibling::*//tr[@class="lowest_price"]/td[2])').get() 
		new_camel_item['lowest_amazon_date'] = response.xpath('normalize-space(//*[@class="pricetype pricetype0"]/following-sibling::*//tr[@class="lowest_price"]/td[3])').get()
		new_camel_item['avg_amazon_price'] = response.xpath('normalize-space(//*[@class="pricetype pricetype0"]/following-sibling::*//tr[@class="lowest_price"]/following-sibling::*/td[2])').get() 
		
		new_camel_item['highest_tercero_price'] = response.xpath('normalize-space(//*[@class="pricetype pricetype1"]/following-sibling::*//tr[@class="highest_price"]/td[2])').get() 
		new_camel_item['highest_tercero_date'] = response.xpath('normalize-space(//*[@class="pricetype pricetype1"]/following-sibling::*//tr[@class="highest_price"]/td[3])').get()
		new_camel_item['lowest_tercero_price'] = response.xpath('normalize-space(//*[@class="pricetype pricetype1"]/following-sibling::*//tr[@class="lowest_price"]/td[2])').get() 
		new_camel_item['lowest_tercero_date'] = response.xpath('normalize-space(//*[@class="pricetype pricetype1"]/following-sibling::*//tr[@class="lowest_price"]/td[3])').get()
		new_camel_item['avg_tercero_price'] = response.xpath('normalize-space(//*[@class="pricetype pricetype1"]/following-sibling::*//tr[@class="lowest_price"]/following-sibling::*/td[2])').get() 

		# new_camel_item['URL_graph'] = response.xpath('normalize-space(//img[@id="summary_chart"]/@src)').get()
		new_camel_item['SKU_graph'] = response.xpath('normalize-space(//td[contains(translate(text(),"ABCDEFGHIJKLMNOPURSTUWXYZ","abcdefghijklmnopurstuwxyz"),"sku")]/following-sibling::*[1])').get() or '-1'
		

		yield new_camel_item

