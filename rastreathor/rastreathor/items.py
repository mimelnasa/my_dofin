# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem
from inspector.models import Product, Seller, SellerFeedback, CamelProduct

class ProductItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # Se definen los campos
    django_model = Product


class SellerItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # Se definen los campos
    django_model = Seller


class SellerFeedbackItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # Se definen los campos
    django_model = SellerFeedback


class CamelItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # Se definen los campos
    django_model = CamelProduct

